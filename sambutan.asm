.model small  
.code  
PUTC    MACRO   char
        PUSH    AX
        MOV     AL, char
        MOV     AH,0Eh
        INT    10H
        POP     AX       
ENDM           
org 100h   
mov ax, 3     
int 10h   
mov ax, 1003h
mov bx, 0
int 10h
mov ax, 0b800h
mov ds, ax
mov [02h], 's'
mov [04h], 'e'
mov [06h], 'l'
mov [08h], 'a'
mov [0ah], 'm'
mov [0ch], 'a'
mov [0eh], 't'
mov [10h], ' '
mov [12h], 'd'
mov [14h], 'a'
mov [16h], 't'
mov [18h], 'a'
mov [1ah], 'n'
mov [1ch], 'g'
mov [1eh], ' '
mov [20h], 'd'
mov [22h], 'i'
mov [24h], ' '
mov [26h], 'p'
mov [28h], 'r'
mov [2ah], 'o'
mov [2ch], 'g'
mov [2eh], 'r'
mov [30h], 'a'   
mov [32h], 'm'
mov [34h], ' '
mov [36h], 's'  
mov [38h], 'e'
mov [3ah], 'd'  
mov [3ch], 'e'
mov [3eh], 'r'
mov [40h], 'h'
mov [42h], 'a'
mov [44h], 'n'
mov [46h], 'a'
mov [48h], ' '
mov [4ah], 'D' 
mov [4ch], 'S'
mov [4eh], 'K'
mov [50h], '"'
mov [52h], '2'
mov [54h], '1'
mov cx,50
mov di,03h
c: mov [di], 10111001b
   add di,2      
loop c  

putc 0ah
input:
mov ah,01h
int 21h 
mov ah,01h
int 21h
mov ah,01h
int 21h
mov ah,01h
int 21h
mov ah,01h
int 21h   
mov ah,01h
int 21h

mov ah, 0
int 16h
ret
